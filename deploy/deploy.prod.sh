#!/usr/bin/env bash

echo "Starting ...";

ROOT_PATH="/var/www/mff2019.org"
cd ${ROOT_PATH} || exit

## push newly added files to git


# pull latest code
echo "> Fetching latest source code ...";
git reset --hard
git pull origin master

# fix path owner & permission
echo "Setting up permissions ...";
find ${ROOT_PATH} -exec chown www-data:www-data {} \;
find ${ROOT_PATH} -type f -exec chmod 644 {} \;
find ${ROOT_PATH} -type d -exec chmod 755 {} \;
chmod 444 -R ${ROOT_PATH}/_
chmod 444 -R ${ROOT_PATH}/deploy
chmod 444 -R ${ROOT_PATH}/.git

sudo service apache2 restart
echo "All is Well ... Bye !";
