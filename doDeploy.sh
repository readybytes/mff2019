#!/usr/bin/env bash
FILE="deploy/deploy.$1.sh"

if [ -f $FILE ]; then
    echo "..."
else
   FILE="deploy.none.sh"
fi

echo "Deploying on ... [$1]";
echo ""

# echo 'Building JS & CSS'
# if [ "$(yarn run production)" ]; then
#   echo '    >> Succesful'
# else
#   echo '    >> Error: Not able to compile the files.';
#   echo '    >> Exiting'; exit 1;
# fi

# echo ""
# echo 'Checking JS & CSS are commited or not'
# if [ -z "$(git status --untracked-files=no --porcelain public)" ]; then
#   # Working directory clean excluding untracked files
#   echo '    >> Working Directory clean'
# else
#   # Uncommitted changes in tracked files
#   echo "    >> Error: There are changes in tracked files, please commit these first."
#   echo "    >> Exiting."
#   exit 1
# fi

echo ""
echo "Deploying $FILE"
echo "------------------------------------------"
ssh root@159.65.164.31 "bash -s" < $FILE
echo "------------------------------------------"
echo ""
echo "Bye Bye"
echo ""
